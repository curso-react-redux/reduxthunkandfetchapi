import React, { Component } from 'react'
import { getUf } from '../redux/uf/ufAction'
import { connect } from 'react-redux'

class UF extends Component {

    componentDidMount() {
        this.props.getUf()
    }

    render() {
        return (
            <table>
                <thead>
                    <tr>
                        <td>
                            Nome
                        </td>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.uf ? (
                            this.props.uf.map((prop, key) => {
                                return <tr key={key}>
                                    <td>
                                        {prop.nome}
                                    </td>
                                </tr>
                            })
                        ) : (
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                            )
                    }
                </tbody>
            </table>
        )
    }
}

const mapStateToProps = state => ({
    uf: state.ufReducer.uf
})

export default connect(mapStateToProps, { getUf })(UF)
