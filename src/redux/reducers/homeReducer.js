

const initial_state = {
    name: "batata"
}

const homeReducer = (state = initial_state, action) => {
    switch (action.type) {
        case "CHANGE_NAME":
            return { name: action.payload }
            break
        default:
            return state

    }
}

export default homeReducer