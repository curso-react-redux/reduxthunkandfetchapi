import { combineReducers } from 'redux'
import homeReducer from './homeReducer'
import ufReducer from './ufReducer'
export default combineReducers({
    homeReducer,
    ufReducer
})