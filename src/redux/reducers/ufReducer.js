import { UF_SUCCESS_GET_API } from "../uf/ufAction";

const initial_state = {
    uf: []
}

const ufReducer = (state = initial_state, action) => {
    switch (action.type) {
        case UF_SUCCESS_GET_API:
            return { uf: action.payload }
            break
        default:
            return state

    }
}

export default ufReducer