import fetch from 'cross-fetch'

export const UF_SUCCESS_GET_API = 'UF_SUCCESS_GET_API'


export const getUf = (event) => {
    return dispatch => {
        getUfApi(dispatch)
    }
}

export const getUfApi = (dispatch) =>{
    let url = "http://servicodados.ibge.gov.br/api/v1/localidades/estados/"
    fetch(url, {
        method: "GET",
    })
    .then(response => response.json())
    .then(json => successGetUfApi(dispatch, json))
}

export const successGetUfApi = (dispatch, response) =>{
    dispatch({type:UF_SUCCESS_GET_API, payload:response})
}