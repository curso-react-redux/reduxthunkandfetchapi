import User from "./pages/User"
import Home from "./pages/Home"
import UF from "./pages/UF";

const routesConfig = [
    {
        path: "/",
        component:Home,
        exact:true
    },
    {
        path: "/user",
        component:User,
        exact:true
    },
    {
        path: "/uf",
        component:UF,
        exact:true
    }
]

export default routesConfig